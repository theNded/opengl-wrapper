//
// Created by Neo on 14/08/2017.
//

#ifndef OPENGL_SNIPPET_CAMERA_H
#define OPENGL_SNIPPET_CAMERA_H

// set K, P
// conversions regarding captured image from Window
#include <GL/glew.h>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <opencv2/opencv.hpp>

#include "window.h"

// into ONE format
namespace gl {
class Camera {
public:
  /// Camera c = Camera(); c.set_perspective( ... )
  /// or Camera c = Camera( ... )
  Camera() : z_near_(0.01f), z_far_(100.0f) {};

  // Should be coincide with the window
  Camera(int width, int height,
         float fov = 45.0f,
         float z_near = 0.01f,
         float z_far = 100.0f);

  /// Projection - intrinsics
  // getter
  const glm::mat4 projection() const { return projection_; }
  cv::Matx33f projection_to_hartley(int width, int height);

  // setter
  void set_projection(glm::mat4 projection) { projection_ = projection; }
  void set_projection(int width, int height,
                      float fov = 45.0f,
                      float z_near = 0.001f, float z_far = 100.0f);
  void set_projection_from_hartley(float fx, float cx, int width,
                                   float fy, float cy, int height,
                                   float z_near = 0.001f, float z_far = 100.0f);
  void set_projection_from_hartley(const cv::Mat_<float> K,
                                   int width, int height,
                                   float z_near = 0.001f, float z_far = 100.0f);


  /// View - extrinsics
  // getter
  const glm::mat4 view() const { return view_; }
  cv::Matx34f view_to_hartley();

  // setter
  void set_view(glm::mat4 view) { view_ = view; }
  void set_view(const cv::Mat_<float> R, const cv::Mat_<float> t);

  /// Model
  const glm::mat4 model() const { return model_; }
  void set_model(glm::mat4 model) { model_ = model; }

  /// mvp
  glm::mat4 mvp() { return projection_ * view_ * model_; }

/**
   * Given 3x4 matrix P, generate intrinsics and extrinsics
   * @param P (3x4)
   * @param width
   * @param height
   * @param z_near
   * @param z_far
   */
  void set_mvp_from_hartley(const cv::Mat_<float> P, int width, int height,
                            float z_near = 0.001f, float z_far = 100.0f);

  // ... Or set it with interactions
  void SwitchInteraction(bool enable_interaction);
  void UpdateView(Window &window);
  cv::Mat ConvertDepthBuffer(cv::Mat &depthf, float factor);

private:
  int width_;
  int height_;
  float fov_;
  float z_near_;
  float z_far_;

  glm::mat4 projection_; // K
  glm::mat4 view_;       // [R | t]
  glm::mat4 model_;      // e.g. {x, -y, -z}

  // Polar-system: interaction from the keyboard
  bool interaction_enabled_;
  glm::vec3 position_;
  float elevation_;
  float azimuth_;
  float kMoveSpeed = 3.0f;
  float kRotateSpeed = 0.5f;
};
} // namespace gl

#endif // OPENGL_SNIPPET_CAMERA_H
