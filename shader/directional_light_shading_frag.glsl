#version 330 core

#define LIGHT_COUNT 1

// Interpolated values from the vertex shaders
in vec3 position_c;
in vec3 normal_c;
in vec3 light_dir_c[LIGHT_COUNT];

uniform float light_power;
uniform vec3 ambient_coeff;
uniform vec3 specular_coeff;
uniform vec3 diffuse_color;
uniform vec3 light_color;

// Ouput data
out vec3 color;

// Values that stay constant for the whole meshing.
void main() {
    color = vec3(0, 0, 0);
    vec3 lambertian_factor = vec3(0);
    vec3 specular_factor   = vec3(0);
	for (int i = 0; i < LIGHT_COUNT; ++i) {
        vec3 n = normalize(normal_c);
        vec3 l = normalize(light_dir_c[i]);
        float cos_theta = clamp(dot(n, l), 0, 1);

        vec3 e = -normalize(position_c);
        vec3 r = reflect(-l, n);
        float cos_alpha = clamp(dot(e, r), 0, 1);

        lambertian_factor += cos_theta * light_power;
        specular_factor   += pow(cos_alpha, 5) * light_power;
	}

	lambertian_factor = clamp(lambertian_factor, 0, 1);
	specular_factor = clamp(specular_factor, 0, 1);

    color =
        // Ambient : simulates indirect lighting
        diffuse_color * ambient_coeff +
        // Diffuse : "color" of the object
        diffuse_color * light_color * lambertian_factor +
        // Specular : reflective highlight, like a mirror
        light_color * specular_coeff * specular_factor;
    color = clamp(color, 0, 1);
}