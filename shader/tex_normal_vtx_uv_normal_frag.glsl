#version 330 core

in vec3 normal;
layout(location = 0) out vec4 out_normal;

void main() {
    out_normal = vec4(normal, 1.0f);
}