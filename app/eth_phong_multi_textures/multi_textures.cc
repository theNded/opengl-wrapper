//
// Created by Neo on 16/7/29.
//

#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "glwrapper.h"
#include "cxxopts.h"

int main(int argc, char** argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0],
                           "Phong shading renderer"
                               "(support at least 1 and most 3 textures)"
                               "Press key 0~2 to switch between textures"
                               "Press key ENTER to take a screenshot");

  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("w,width", "image width", cxxopts::value<int>()->default_value("640"))
      ("h,height", "image height", cxxopts::value<int>()->default_value("480"))
      ("n,name", "window name",
       cxxopts::value<std::string>()->default_value("Phong with textures"))
      ("l,light", "path-to-config recording lighting conditions",
       cxxopts::value<std::string>()->default_value("lights.yaml"))
      ("m,model", "path-to-model (.obj)",
       cxxopts::value<std::string>()
           ->default_value("../../model/bunny/model.obj"))
      ("t,texture", "path-to-texture0 (.png)",
       cxxopts::value<std::string>()
           ->default_value("../../model/bunny/Texture20_dilated_5.png"))
      ("texture1", "path-to-texture1 (.png)",
       cxxopts::value<std::string>()
           ->default_value("../../model/bunny/Albedo000_dilated_5.png"))
      ("texture2", "path-to-texture2 (.png)",
       cxxopts::value<std::string>()
           ->default_value("../../model/bunny/Albedo145_dilated_5.png"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_model, path_to_light, window_name;
  std::vector<std::string> path_to_textures;
  int width = 640, height = 480;
  if (result.count("model")) {
    path_to_model = result["model"].as<std::string>();
  }
  if (result.count("texture")) {
    path_to_textures.emplace_back(result["texture"].as<std::string>());
  }
  if (result.count("texture1")) {
    path_to_textures.emplace_back(result["texture1"].as<std::string>());
  }
  if (result.count("texture2")) {
    path_to_textures.emplace_back(result["texture2"].as<std::string>());
  }
  if (result.count("light")) {
    path_to_light = result["light"].as<std::string>();
  }
  if (result.count("width")) {
    width = result["width"].as<int>();
  }
  if (result.count("height")) {
    height = result["height"].as<int>();
  }
  if (result.count("name")) {
    window_name = result["name"].as<std::string>();
  }

  gl::Model model;
  model.LoadObj(path_to_model);
  gl::Light lights;
  lights.Load(path_to_light);

  gl::Window window(window_name, width, height);
  gl::Camera camera(window.visual_width(), window.visual_height());
  glm::mat4 model_mat = glm::mat4(1.0f);
  model_mat[1][1] = model_mat[2][2] = -1;
  camera.set_model(model_mat);
  camera.SwitchInteraction(true);

  gl::Texture texture[3];
  for (size_t i = 0; i < path_to_textures.size(); ++i) {
    texture[i].Init(path_to_textures[i]);
  }

  gl::Program program;
  program.Load("img_vtx_uv_normal_uni_tex_light_vert.glsl", gl::kVertexShader);
  program.ReplaceMacro("LIGHT_COUNT", lights.count_str, gl::kVertexShader);
  program.Load("img_vtx_uv_normal_uni_tex_light_frag.glsl", gl::kFragmentShader);
  program.ReplaceMacro("LIGHT_COUNT", lights.count_str, gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "view", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "texture_sampler", gl::kTexture2D);
  uniforms.GetLocation(program.id(), "light", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_power", gl::kFloat);
  uniforms.GetLocation(program.id(), "ambient_coeff", gl::kVector3f);
  uniforms.GetLocation(program.id(), "specular_coeff", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_color", gl::kVector3f);

  gl::Args args(4);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.positions().size(), model.positions().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.normals().size(), model.normals().data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                  model.uvs().size(), model.uvs().data());
  args.BindBuffer(3, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  int texture_id = 0;
  int screenshot_idx = 0;
  do {
    if (window.get_key(GLFW_KEY_0) == GLFW_PRESS) {
      texture_id = 0;
    } else if (window.get_key(GLFW_KEY_1) == GLFW_PRESS) {
      texture_id = 1;
    } else if (window.get_key(GLFW_KEY_2) == GLFW_PRESS) {
      texture_id = 2;
    }
    if (texture_id >= path_to_textures.size()) {
      std::cout << "Invalid texture id; fall back to default texture"
                << std::endl;
      texture_id = 0;
    }
    if (window.get_key(GLFW_KEY_N)) {
      screenshot_idx ++;
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.UpdateView(window);
    glm::mat4 mvp = camera.mvp();
    glm::mat4 view = camera.view();

    glUseProgram(program.id());
    texture[texture_id].Bind(0);
    uniforms.Bind("mvp", &mvp, 1);
    uniforms.Bind("view", &view, 1);
    uniforms.Bind("texture_sampler", GLuint(0));
    uniforms.Bind("light", lights.positions.data(), lights.count);
    uniforms.Bind("light_power", &lights.power, 1);
    uniforms.Bind("ambient_coeff", &lights.ambient_coeff, 1);
    uniforms.Bind("specular_coeff", &lights.specular_coeff, 1);
    uniforms.Bind("light_color", &lights.light_color, 1);

    glBindVertexArray(args.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    if (window.get_key(GLFW_KEY_ENTER)) {
      std::stringstream ss;
      ss << "./" << screenshot_idx << "-" << texture_id << ".png";
      cv::imwrite(ss.str(), window.CaptureRGB());
    }
    window.swap_buffer();
  } while( window.get_key(GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
           window.should_close() == 0 );

  // Close OpenGL window and terminate GLFW
  glfwTerminate();

  return 0;
}