//
// Created by wei on 17-12-20.
//

#include <iostream>
#include <cxxopts.h>

int main(int argc, char* argv[])
{
  cxxopts::Options options(argv[0], "TestOpt");
  options
      .positional_help("")
      .show_positional_help();
  options.add_options()
      ("help", "print help message")
      ("m,model", ".obj path",
       cxxopts::value<std::string>()->default_value("../../model/bunny/model.obj"))
      ("w,width",  "atlas width",  cxxopts::value<int>()->default_value("640"))
      ("h,height", "atlas height", cxxopts::value<int>()->default_value("480"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    exit(0);
  }
  if (result.count("model")) {
    std::cout << result["model"].as<std::string>() << std::endl;
  }
  if (result.count("width")) {
    std::cout << result["width"].as<int>() << std::endl;
  }
  if (result.count("height")) {
    std::cout << result["height"].as<int>() << std::endl;
  }
  return 0;
}