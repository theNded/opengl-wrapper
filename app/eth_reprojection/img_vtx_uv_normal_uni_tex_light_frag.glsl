#version 330 core

// Ouput data
layout(location = 0) out vec4 color;

flat in float idx;
in vec3 position;
uniform int render_color;

void main() {
  color = (render_color != 0) ? vec4(idx, position) : vec4(0);
}