//
// Created by wei on 18-1-19.
//

// Include standard headers
#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <string>
#include <vector>

#include "cube.h"
#include "glwrapper.h"
#include "rect.h"

int main() {
  gl::Window window("Cube", 640, 480);
  gl::Camera camera(window.visual_width(), window.visual_height());
  camera.SwitchInteraction(true);

  gl::Program program;
  program.Load("img_vtx_color_vert.glsl", gl::kVertexShader);
  program.Load("img_vtx_color_frag.glsl", gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "render_color", gl::kInt);

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3), 8,
                  kPositions);
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3), 8, kColors);
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_BYTE),
                  36, vIndices);
  gl::FrameBuffer fbo(GL_RGB, window.visual_width(), window.visual_height());

  gl::Program program_pass2;
  program_pass2.Load("img_uni_tex_vert.glsl", gl::kVertexShader);
  program_pass2.Load("img_uni_tex_frag.glsl", gl::kFragmentShader);
  program_pass2.Build();

  gl::Uniforms uniforms_pass2;
  uniforms_pass2.GetLocation(program_pass2.id(), "tex", gl::kTexture2D);

  gl::Args args_pass2(1);
  args_pass2.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3), 6,
                        (void *)kVertices);

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);

  do {
    camera.UpdateView(window);
    fbo.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDepthFunc(GL_LEQUAL);

    glm::mat4 mvp = camera.mvp();
    int render_color;

    glUseProgram(program.id());
    uniforms.Bind("mvp", &mvp, 1);

    // NOW WE CAN GET VISIBILITY
    glBindVertexArray(args.vao());
    render_color = 0;
    uniforms.Bind("render_color", &render_color, 1);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, 0);    

    render_color = 1;
    uniforms.Bind("render_color", &render_color, 1);
    glDrawArrays(GL_POINTS, 0, 8);
    glBindVertexArray(0);

    window.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program_pass2.id());
    fbo.texture().Bind(1);
    uniforms_pass2.Bind("tex", GLuint(1));
    glBindVertexArray(args_pass2.vao());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);  

    window.swap_buffer();
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           window.should_close() == 0);

  glfwTerminate();

  return 0;
}
