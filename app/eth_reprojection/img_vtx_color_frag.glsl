#version 330 core

layout(location = 0) out vec3 final_color;

in vec3 color;
uniform int render_color;

void main() {
  final_color = (render_color != 0) ? vec3(1) : vec3(color);
}