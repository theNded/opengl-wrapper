#version 330 core

layout(location = 0) in vec3 in_position;
layout(location = 1) in float in_index;

flat out float idx;
out vec3 position;
uniform mat4 mvp;

void main() {
    // clip coordinate
    gl_Position =  mvp * vec4(in_position, 1.0);
    position = in_position;
    idx = in_index;
}

