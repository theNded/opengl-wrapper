//
// Created by Neo on 16/7/29.
//

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "cxxopts.h"
#include "glwrapper.h"

int main(int argc, char **argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0], "Phong shading renderer (with texture)");
  options.positional_help("").show_positional_help();
  options.add_options()("help", "show usages")(
      "w,width", "image width", cxxopts::value<int>()->default_value("640"))(
      "h,height", "image height", cxxopts::value<int>()->default_value("480"))(
      "n,name", "window name",
      cxxopts::value<std::string>()->default_value("Phong Textured"))(
      "l,light", "path-to-config recording lighting conditions",
      cxxopts::value<std::string>()->default_value("lights.yaml"))(
      "m,model", "path-to-model (.obj)",
      cxxopts::value<std::string>()->default_value(
          "../../model/face/model.obj"))(
      "t,texture", "path-to-texture (.png)",
      cxxopts::value<std::string>()->default_value(
          "../../model/face/texture.png"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_model, path_to_texture, path_to_light, window_name;
  int width = 1280, height = 960;
  if (result.count("model")) {
    path_to_model = result["model"].as<std::string>();
  }
  if (result.count("texture")) {
    path_to_texture = result["texture"].as<std::string>();
  }
  if (result.count("light")) {
    path_to_light = result["light"].as<std::string>();
  }
  if (result.count("width")) {
    width = result["width"].as<int>();
  }
  if (result.count("height")) {
    height = result["height"].as<int>();
  }
  if (result.count("name")) {
    window_name = result["name"].as<std::string>();
  }

  gl::Model model;
  model.LoadObj(path_to_model);
  gl::Light lights;
  lights.Load(path_to_light);

  // Context and control init
  gl::Window window(window_name, width, height);
  gl::Camera camera(window.visual_width(), window.visual_height());
  camera.SwitchInteraction(false);
  camera.LoadHartleyIntrinsic("../../model/face/intrinsic_single.txt");
  camera.LoadExtrinsics("../../model/face/extrinsics.txt");

  gl::Texture texture;
  texture.Init(path_to_texture);

  gl::Program program;
  program.Load("img_vtx_uv_normal_uni_tex_light_vert.glsl", gl::kVertexShader);
  program.Load("img_vtx_uv_normal_uni_tex_light_frag.glsl",
               gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "render_color", gl::kInt);

  std::vector<float> indices;
  for (size_t i = 0; i < model.positions().size(); ++i) {
    indices.emplace_back(float(i));
  }

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.positions().size(), model.positions().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT), indices.size(),
                  indices.data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo(GL_RGBA32F, window.visual_width(),
                      window.visual_height());

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  cv::Mat m;
  cv::Mat im;
  for (int iter = 0; iter < 1; ++iter) {
    window.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /// Update viewpoint
    camera.UpdateView(window);
    glm::mat4 mvp = camera.projection() * camera.extrinsics()[0];

    glUseProgram(program.id());
    uniforms.Bind("mvp", &mvp, 1);

    int render_color = 0;
    uniforms.Bind("render_color", &render_color, 1);
    glBindVertexArray(args.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    
    render_color = 1;
    uniforms.Bind("render_color", &render_color, 1);
    glDrawArrays(GL_POINTS, 0, model.positions().size());
    glBindVertexArray(0);

    //cv::imwrite("test1.png", window.CaptureRGB());

    gl::Model model_i;
    model_i.positions().clear();
    model_i.uvs().clear();
    model_i.normals().clear();
    model_i.indices().clear();

    std::unordered_map<int, glm::vec2> visible_vertices;
    std::unordered_map<int, int> new_vertex_indices;
    m = fbo.Capture();

    im = cv::Mat(m.rows, m.cols, CV_8UC1);    
    for (int i = 0; i < m.rows; ++i) {
      for (int j = 0; j < m.cols; ++j) {
        // c[0] -- begin with 0
        cv::Vec4f c = m.at<cv::Vec4f>(i, j);        
        if (c[0] != 0) {
          visible_vertices.emplace(
              c[0], glm::vec2(float(i) / m.rows, float(m.cols - j) / m.cols));
/*           assert(fabsf(model.positions()[c[0]].x - c[1] < 1e-7));
          assert(fabsf(model.positions()[c[0]].y - c[2] < 1e-7));
          assert(fabsf(model.positions()[c[0]].z - c[3] < 1e-7)); */
          im.at<unsigned char>(i, j) = 255;
        } else {
          im.at<unsigned char>(i, j) = 0;
        }
      }
    }
    cv::imwrite("test.png", im);
/* 
    int new_vertex_cnt = 0;
    for (int i = 0; i < model.indices().size(); i += 3) {
      // model.indices -- begin with 1
      int idx[3] = {model.indices()[i + 0] - 1, model.indices()[i + 1] - 1,
                    model.indices()[i + 2] - 1};

      bool foundx = visible_vertices.find(idx[0]) != visible_vertices.end();
      bool foundy = visible_vertices.find(idx[1]) != visible_vertices.end();
      bool foundz = visible_vertices.find(idx[2]) != visible_vertices.end();

      // triangle exists
      if (foundx && foundy && foundz) {
        for (int j = 0; j < 3; ++j) {
          if (new_vertex_indices.find(idx[j]) == new_vertex_indices.end()) {
            new_vertex_indices.emplace(idx[j], new_vertex_cnt);

            model_i.positions().emplace_back(
                glm::vec3(model.uvs()[idx[j]], 1.0f));
            model_i.normals().emplace_back(model.normals()[idx[j]]);
            model_i.uvs().emplace_back(visible_vertices[idx[j]]);

            new_vertex_cnt++;
          }
        }

        if (idx[0] == idx[1] || idx[1] == idx[2] || idx[2] == idx[0]) {
          std::cout << idx[0] << " " << idx[1] << " " << idx[2] << std::endl;
        }
        model_i.indices().emplace_back(new_vertex_indices[idx[0]] + 1);
        model_i.indices().emplace_back(new_vertex_indices[idx[1]] + 1);
        model_i.indices().emplace_back(new_vertex_indices[idx[2]] + 1);
        if (new_vertex_indices[idx[0]] == new_vertex_indices[idx[1]] ||
            new_vertex_indices[idx[1]] == new_vertex_indices[idx[2]] ||
            new_vertex_indices[idx[2]] == new_vertex_indices[idx[0]]) {
          std::cout << idx[0] << " " << idx[1] << " " << idx[2] << std::endl;
        }
      }
    }
    model_i.SaveObj("test.obj"); */

    window.swap_buffer();
  }

  glfwTerminate();

  return 0;
}