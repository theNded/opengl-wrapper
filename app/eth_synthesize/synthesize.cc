//
// Created by Neo on 21/09/2017.
//

/// In Super Resolution:
/// Input: low-res texture -> low-res images
///        projection matrices of [high-res texture -> high-res image]
/// Output: high-res texture, high-res images
/// (We should generate these ground truth)

#include <chrono>
#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "glwrapper.h"
#include <iomanip>
#include <opencv2/opencv.hpp>

#include "config_loader.h"
#include "cxxopts.h"
#include "encode_pixel2uv.h"

/// Wrap for light sources
/// Show axis and light sources
std::string kConfigPath = ".";
std::string kProjectionPath = "./gen_calib";

static const GLfloat kVertices[] = {
    -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, -1.0f, 1.0f, 0.0f,
    -1.0f, 1.0f,  0.0f, 1.0f, -1.0f, 0.0f, 1.0f,  1.0f, 0.0f,
};

int main(int argc, char **argv) {
  //////////////////////////////////////////////////
  /// Path & factor config
  ConfigLoader config;
  config.LoadParams(kConfigPath + "/params.yaml");
  std::string output_path = config.output_path;
  int factor = config.downsample_factor;
  int image_width = config.output_width_original;
  int image_height = config.output_height_original;
  if (factor <= 1) {
    std::cout << "No downsample, early return!\n";
    return 0;
  }

  //////////////////////////////////////////////////
  /// Light config

  //////////////////////////////////////////////////
  /// mkdir
  system(std::string("mkdir " + output_path).c_str());
  system(std::string("mkdir " + output_path + "/atlas").c_str());
  system(std::string("mkdir " + output_path + "/projection").c_str());
  system(std::string("mkdir " + output_path + "/image").c_str());
  system(std::string("mkdir " + output_path + "/vagia").c_str());
  system(std::string("mkdir " + output_path + "/vagia/Frame000").c_str());

  std::stringstream ss;
  ss.str("");
  ss << "mkdir " << output_path << "/atlas/gt";
  system(ss.str().c_str());

  ss.str("");
  ss << "mkdir " << output_path << "/atlas/factor_" << factor;
  system(ss.str().c_str());

  ss.str("");
  ss << "mkdir " << output_path << "/image/factor_" << factor;
  system(ss.str().c_str());

  //// For Vagia
  ss.str("");
  ss << "mkdir " << output_path << "/vagia/Frame000/factor_" << factor;
  system(ss.str().c_str());

  //////////////////////////////////////////////////
  /// GL config
  gl::Window window("Synthesizer", image_width, image_height);
  gl::Camera camera(window.visual_width(), window.visual_height());
  camera.SwitchInteraction(true);
  camera.LoadHartleyIntrinsic(kProjectionPath + "/intrinsic.txt");
  gl::Trajectory trajectory;
  trajectory.LoadExtrinsics(kProjectionPath + "/extrinsics.txt");

  /// Model & texture config
  gl::Model model;
  model.LoadObj(config.input_model_path);
  gl::Texture input_texture;
  input_texture.Init(config.input_texture_path);
  gl::Light lights;
  lights.Load("./lights_0.yaml");

  ////////////////////////////////////
  /// uv writer
  gl::Program program0;
  program0.Load("img_uv_vtx_uv_vert.glsl", gl::kVertexShader);
  program0.Load("img_uv_vtx_uv_frag.glsl", gl::kFragmentShader);
  program0.Build();
  gl::Uniforms uniforms0;
  uniforms0.GetLocation(program0.id(), "mvp", gl::kMatrix4f);
  gl::Args args0(3);
  args0.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                   model.positions().size(), model.positions().data());
  args0.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                   model.uvs().size(), model.uvs().data());
  args0.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                   model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_uv(GL_RGBA32F, image_width, image_height);

  ////////////////////////////////////
  /// renderer - 2 passes
  gl::Program program1;
  program1.Load("img_vtx_uv_normal_uni_tex_light_vert.glsl", gl::kVertexShader);
  program1.ReplaceMacro("LIGHT_COUNT", lights.count_str, gl::kVertexShader);
  program1.Load("img_vtx_uv_normal_uni_tex_light_frag.glsl",
                gl::kFragmentShader);
  program1.ReplaceMacro("LIGHT_COUNT", lights.count_str, gl::kFragmentShader);
  program1.Build();

  gl::Uniforms uniforms1;
  uniforms1.GetLocation(program1.id(), "mvp", gl::kMatrix4f);
  uniforms1.GetLocation(program1.id(), "view", gl::kMatrix4f);
  uniforms1.GetLocation(program1.id(), "texture_sampler", gl::kTexture2D);
  uniforms1.GetLocation(program1.id(), "light", gl::kVector3f);
  uniforms1.GetLocation(program1.id(), "light_power", gl::kFloat);
  uniforms1.GetLocation(program1.id(), "ambient_coeff", gl::kVector3f);
  uniforms1.GetLocation(program1.id(), "specular_coeff", gl::kVector3f);
  uniforms1.GetLocation(program1.id(), "light_color", gl::kVector3f);

  gl::Args args1(4);
  args1.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                   model.positions().size(), model.positions().data());
  args1.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                   model.normals().size(), model.normals().data());
  args1.BindBuffer(2, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                   model.uvs().size(), model.uvs().data());
  args1.BindBuffer(3, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                   model.indices().size(), model.indices().data());
  // gl::FrameBuffer fbo_image(GL_RGB, image_width, image_height);
  //
  //  gl::Program program2;
  //  program2.Load("../shader/img_uni_tex_vert.glsl", gl::kVertexShader);
  //  program2.Load("../shader/img_uni_tex_frag.glsl", gl::kFragmentShader);
  //  program2.Build();
  //  gl::Uniforms uniforms2;
  //  uniforms2.GetLocation(program2.id(), "tex", gl::kTexture2D);
  //  gl::Args args2(2);
  //  args2.BindBuffer(0, {GL_ARRAY_BUFFER, sizeof(float), 3, GL_FLOAT},
  //                   6, (void*)kVertices);
  //  args2.BindBuffer(1, {GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int),
  //                       1, GL_UNSIGNED_INT},
  //                   model.indices().size(), model.indices().data());

  // Additional settings
  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  size_t i = 0;
  do {
    if (i >= trajectory.extrinsics().size())
      break;

    if (!config.use_preset_path) {
      camera.UpdateView(window);
    } else {
      camera.set_view(trajectory.extrinsics()[i]);
    }

    glm::mat4 mvp = camera.mvp();
    glm::mat4 view = camera.view();

    // Pass 0: render uv-coordinates in FBO
    fbo_uv.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program0.id());
    input_texture.Bind(0);
    uniforms0.Bind("mvp", &mvp, 1);
    glBindVertexArray(args0.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // Pass 1: render image in FBO
    window.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program1.id());
    input_texture.Bind(0);
    uniforms1.Bind("mvp", &mvp, 1);
    uniforms1.Bind("view", &view, 1);
    uniforms1.Bind("texture_sampler", GLuint(0));
    uniforms1.Bind("light", lights.positions.data(), lights.count);
    uniforms1.Bind("light_power", &lights.power, 1);
    uniforms1.Bind("ambient_coeff", &lights.ambient_coeff, 1);
    uniforms1.Bind("specular_coeff", &lights.specular_coeff, 1);
    uniforms1.Bind("light_color", &lights.light_color, 1);

    glBindVertexArray(args1.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // Pass 2: render to window for visualization
    //    window.Bind();
    //    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //    glUseProgram(program2.id());
    //    fbo_image.texture().Bind(1);
    //    uniforms2.Bind("tex", GLuint(1));
    //    glBindVertexArray(args2.vao());
    //    glDrawArrays(GL_TRIANGLES, 0, 6);
    //    glBindVertexArray(0);

    ss.str("");
    ss << i;
    std::string i_str = ss.str();
    ss.str("");
    ss << i + 1;
    std::string i_1_str = ss.str();
    ss.str("");
    ss << factor;
    std::string factor_str = ss.str();

    EncodePixelToUV(output_path + "/projection/map_" + i_str + ".txt",
                    fbo_uv.Capture());
    cv::Mat im = window.CaptureRGB();
    cv::imwrite(output_path + "/projection/image_" + i_str + ".png", im);

    cv::Mat im_down;
    cv::resize(im, im_down, cv::Size(im.cols / factor, im.rows / factor));
    cv::imwrite(output_path + "/image/factor_" + factor_str + "/image_" +
                    i_str + ".png",
                im_down);
    cv::imwrite(output_path + "/vagia/Frame000/factor_" + factor_str +
                    "/Image" + i_1_str + ".png",
                im_down);

    window.swap_buffer();
    ++i;
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           window.should_close() == 0);

  gl::Program program_shading;
  program_shading.Load("tex_shading_vtx_uv_normal_vert.glsl",
                       gl::kVertexShader);
  program_shading.Load("tex_shading_vtx_uv_normal_frag.glsl",
                       gl::kFragmentShader);
  program_shading.ReplaceMacro("LIGHT_COUNT", lights.count_str,
                               gl::kFragmentShader);
  program_shading.Build();

  gl::Uniforms uniforms_shading;
  uniforms_shading.GetLocation(program_shading.id(), "light", gl::kVector3f);
  uniforms_shading.GetLocation(program_shading.id(), "light_power", gl::kFloat);
  uniforms_shading.GetLocation(program_shading.id(), "light_color",
                               gl::kVector3f);
  uniforms_shading.GetLocation(program_shading.id(), "ambient_coeff",
                               gl::kVector3f);

  gl::Args args_shading(4);
  args_shading.BindBuffer(0, {GL_ARRAY_BUFFER, sizeof(float), 2, GL_FLOAT},
                          model.uvs().size(), model.uvs().data());
  args_shading.BindBuffer(1, {GL_ARRAY_BUFFER, sizeof(float), 3, GL_FLOAT},
                          model.positions().size(), model.positions().data());
  args_shading.BindBuffer(2, {GL_ARRAY_BUFFER, sizeof(float), 3, GL_FLOAT},
                          model.normals().size(), model.normals().data());
  args_shading.BindBuffer(
      3, {GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int), 1, GL_UNSIGNED_INT},
      model.indices().size(), model.indices().data());
  int window_width = input_texture.width();
  int window_height = input_texture.height();
  gl::FrameBuffer fbo_shading(GL_RGB, window_width, window_height);
  window.Resize(window_width, window_height);

  fbo_shading.Bind();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  input_texture.Bind(0);
  glUseProgram(program_shading.id());

  uniforms_shading.Bind("lights", lights.positions.data(), lights.count);
  uniforms_shading.Bind("light_power", &lights.power, 1);
  uniforms_shading.Bind("light_color", &lights.light_color, 1);
  uniforms_shading.Bind("ambient_coeff", &lights.ambient_coeff, 1);

  glBindVertexArray(args_shading.vao());
  glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);

  window.swap_buffer();

  // Low resolution atlas
  cv::Mat &shading = fbo_shading.Capture();
  cv::Mat albedo;
  cv::flip(input_texture.image(), albedo, 0);
  cv::Mat composed = cv::Mat(albedo.rows, albedo.cols, CV_8UC3);
  for (int i = 0; i < composed.rows; ++i) {
    for (int j = 0; j < composed.cols; ++j) {
      cv::Vec3b r = albedo.at<cv::Vec3b>(i, j);
      float s = shading.at<cv::Vec3b>(i, j)[0];
      composed.at<cv::Vec3b>(i, j) =
          cv::Vec3b(r[0] * s / 255.0f, r[1] * s / 255.0f, r[2] * s / 255.0f);
    }
  }
  // Upsample them:
  cv::imwrite(output_path + "/atlas/gt/gt_albedo.png", albedo);
  cv::imwrite(output_path + "/atlas/gt/gt_shading.png", shading);
  cv::imwrite(output_path + "/atlas/gt/gt_texture.png", composed);

  cv::Mat down, up;
  cv::resize(composed, down,
             cv::Size(composed.cols / factor, composed.rows / factor));
  cv::resize(down, up, cv::Size(composed.cols, composed.rows));
  ss.str("");
  ss << output_path << "/atlas/factor_" << factor << "/init_texture.png";
  cv::imwrite(ss.str(), up);

  return 0;
}