//
// Created by Neo on 08/08/2017.
//

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <opencv2/opencv.hpp>
#include "glwrapper.h"
#include "cxxopts.h"

int main(int argc, char **argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0], "Normal atlas generation");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("w,width",  "atlas width", cxxopts::value<int>()->default_value("1280"))
      ("h,height", "atlas height", cxxopts::value<int>()->default_value("960"))
      ("m,model",  "path-to-model (.obj)",
       cxxopts::value<std::string>()
           ->default_value("../../model/buddha/model.obj"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_model;
  int atlas_width = 1280, atlas_height = 960;
  if (result.count("model")) {
    path_to_model = result["model"].as<std::string>();
  }
  if (result.count("width")) {
    atlas_width = result["width"].as<int>();
  }
  if (result.count("height")) {
    atlas_height = result["height"].as<int>();
  }

  gl::Model model;
  model.LoadObj(path_to_model);
  gl::Window window("Normal", atlas_width/2, atlas_height/2);
  gl::Program program;
  program.Load("tex_normal_vtx_uv_normal_vert.glsl", gl::kVertexShader);
  program.Load("tex_normal_vtx_uv_normal_frag.glsl", gl::kFragmentShader);
  program.Build();

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                  model.uvs().size(), model.uvs().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.normals().size(), model.normals().data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_normal(GL_RGBA32F, atlas_width, atlas_height);

  fbo_normal.Bind();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glUseProgram(program.id());

  glBindVertexArray(args.vao());
  glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);

  window.swap_buffer();

  cv::Mat normal_atlas = fbo_normal.Capture();
  cv::Mat normal_visualize = cv::Mat(normal_atlas.rows, normal_atlas.cols, CV_8UC4);
  cv::Mat mask = cv::Mat::zeros(normal_atlas.rows, normal_atlas.cols, CV_8U);

  std::ofstream out("normal_atlas.txt");
  std::stringstream ss;
  for (int i = 0; i < normal_atlas.rows; ++i) {
    for (int j = 0; j < normal_atlas.cols; ++j) {
      cv::Vec4f n = normal_atlas.at<cv::Vec4f>(i, j);
      if (n[3] != 0) { // alpha channel
        ss.str("");
        float norm = sqrtf(n[0]*n[0] + n[1]*n[1] +n[2]*n[2]);
        n[0] /= norm; n[1] /= norm; n[2] /= norm;
        normal_atlas.at<cv::Vec4f>(i, j) = n;

        ss << i << " " << j << " "
           << n[0] << " " << n[1] << " " << n[2] << std::endl;
        out << ss.str();

        n[0] = 0.5f * n[0] + 0.5f;
        n[1] = 0.5f * n[1] + 0.5f;
        n[2] = 0.5f * n[2] + 0.5f;

        mask.at<unsigned char>(i, j) = 255;
      }
      normal_visualize.at<cv::Vec4b>(i, j)
          = cv::Vec4b(n[0]*255, n[1]*255, n[2]*255, n[3]*255);
    }
  }
  std::cout << "normal_atlas.txt written." << std::endl;

  cv::cvtColor(normal_visualize, normal_visualize, CV_BGRA2RGBA);
  cv::imwrite("normal_atlas.png", normal_visualize);
  cv::imwrite("mask_atlas.png", mask);
  std::cout << "[normal_atlas|mask_atlas].png written." << std::endl;
  return 0;
}