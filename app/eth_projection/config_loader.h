//
// Created by wei on 18-1-1.
//

#ifndef OPENGL_WRAPPER_CONFIG_LOADER_H
#define OPENGL_WRAPPER_CONFIG_LOADER_H

#include <string>
#include <opencv2/opencv.hpp>

struct ConfigLoader {
  void Load(std::string config_path) {
    cv::FileStorage fs(config_path, cv::FileStorage::READ);
    texture_width = (int)fs["texture_width"];
    std::cout << texture_width << std::endl;
    texture_height = (int)fs["texture_height"];
    intrinsic_config = (int)fs["intrinsic_config"];
    dataset_path = (std::string)fs["dataset_path"];
    std::cout << dataset_path << std::endl;
  }

  std::string dataset_path;
  int texture_width;
  int texture_height;
  int intrinsic_config;
};

#endif //OPENGL_WRAPPER_CONFIG_LOADER_H
