//
// Created by Neo on 21/09/2017.
//

/// Input: mesh(.obj), texture(.png)
///        intrinsic(s) (fx, cx, fy, cy, w, h)
///        extrinsics (4x4 mat)
/// Output: rendered image(.png)
///         pixel-wise texture coordinate map(.txt)
///         => interpreted by MATLAB

#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <opencv2/opencv.hpp>
#include "glwrapper.h"
#include "cxxopts.h"

#include "rect.h"
#include "encode_pixel2uv.h"
#include "config_loader.h"

int main(int argc, char **argv) {
  cxxopts::Options options(
      argv[0], "Generate I(px, py) = (tx, ty) correspondences that "
          "support the building of large sparse MATLAB projection matrices.");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("c,config", "path to configuration (.yaml), specifying details",
       cxxopts::value<std::string>()->default_value("face.yaml"))
      ("o,output", "output path",
       cxxopts::value<std::string>()->default_value("output"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  ConfigLoader config_loader;
  std::string config_path, output_path;
  if (result.count("config")) {
    config_path = result["config"].as<std::string>();
    config_loader.Load(config_path);
  }
  if (result.count("output")) {
    output_path = result["output"].as<std::string>();
  }

  /// Load model
  gl::Model model;
  model.LoadObj(config_loader.dataset_path + "/model.obj");

  const int texture_width = config_loader.texture_width;
  const int texture_height = config_loader.texture_height;
  const int window_width = texture_width / 2;
  const int window_height = texture_height / 2;

  /// Configure OpenGL context
  gl::Window window("Projection", window_width, window_height, false);
  gl::Camera camera(window.visual_width(), window.visual_height());
  camera.SwitchInteraction(false);

  /// Select dataset with different K configurations
  switch (config_loader.intrinsic_config) {
    /// ONLY ONE K is provided for ALL viewpoints,
    /// with fx, fy, cx, cy
    /// They are converted to proper GL intrinsics according to window size
    case 0:
      camera.LoadHartleyIntrinsic(
          config_loader.dataset_path + "/intrinsic_single.txt");
      break;

    /// for EACH viewpoint, K is provided by k11, k12, ... k32, k33 (9 coeffs)
    /// k11, k13, k22, k23 are selected.
    /// They are converted to proper GL intrinsics according to window size
    case 1:
      camera.LoadHartleyIntrinsics(
          config_loader.dataset_path + "/intrinsics.txt", true);
      break;

    /// for EACH viewpoint, focal length is provided
    /// it is directly used to generate GL intrinsics with glPerspective
    /// according to window size
    case 2:
      camera.LoadFocals(
          config_loader.dataset_path + "/intrinsic_focals.txt");
      {
        // A coordinate transform for model is necessary
        glm::mat4 model_mat = glm::mat4(1.0f);
        model_mat[1][1] = model_mat[2][2] = -1;
        camera.set_model(model_mat);
      }
      break;
    default:break;
  }

  /// Each viewpoint a 4x4 world-to-camera matrix is provided
  camera.LoadExtrinsics(config_loader.dataset_path + "/extrinsics.txt");

  gl::Texture input_texture;
  input_texture.Init(config_loader.dataset_path + "/texture.png");

  ////////////////////
  /// Shader 0:
  /// - Find accurate per-pixel correspondence, i.e. I(pu, pv) = (tu, tv).
  /// - The EXACT float value is stored in framebuffer without losing precision.
  gl::Program program0;
  program0.Load("img_uv_vtx_uv_vert.glsl", gl::kVertexShader);
  program0.Load("img_uv_vtx_uv_frag.glsl", gl::kFragmentShader);
  program0.Build();

  gl::Uniforms uniforms0;
  uniforms0.GetLocation(program0.id(), "mvp", gl::kMatrix4f);

  gl::Args args0(3);
  args0.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                   model.positions().size(), model.positions().data());
  args0.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                   model.uvs().size(), model.uvs().data());
  args0.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                   model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_uv(GL_RGBA32F, texture_width, texture_height);

  ////////////////////
  /// Shader 1:
  /// - Traditional rendering
  /// - Render into framebuffer as a texture, since the image resolution
  ///   might be higher than screen size (e.g. 2560x1920)
  gl::Program program1;
  program1.Load("img_vtx_uv_uni_tex_vert.glsl", gl::kVertexShader);
  program1.Load("img_vtx_uv_uni_tex_frag.glsl", gl::kFragmentShader);
  program1.Build();

  gl::Uniforms uniforms1;
  uniforms1.GetLocation(program1.id(), "mvp", gl::kMatrix4f);
  uniforms1.GetLocation(program1.id(), "tex", gl::kTexture2D);

  gl::Args args1(3);
  args1.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                   model.positions().size(), model.positions().data());
  args1.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                   model.uvs().size(), model.uvs().data());
  args1.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                   model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_image(GL_RGB, texture_width, texture_height);

  ////////////////////
  /// Shader 2:
  /// - Render to OpenGL window
  /// - Only for visualization and could be disabled.
//  gl::Program program2;
//  program2.Load("img_uni_tex_vert.glsl", gl::kVertexShader);
//  program2.Load("img_uni_tex_frag.glsl", gl::kFragmentShader);
//  program2.Build();
//
//  gl::Uniforms uniforms2;
//  uniforms2.GetLocation(program2.id(), "tex", gl::kTexture2D);
//
//  gl::Args args2(2);
//  args2.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
//                   6, (void *) kVertices);
//  args2.BindBuffer(1, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
//                   model.indices().size(), model.indices().data());

  /// Additional settings
  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  system(std::string("mkdir -p " + output_path).c_str());
  cv::Mat t = input_texture.image();
  cv::flip(t, t, 0);
  cv::imwrite(output_path + "/texture.png", t);

  /// Iterate viewpoints
  const size_t viewpoint_count = camera.extrinsics().size();
  for (size_t i = 0; i < viewpoint_count; ++i) {
    std::cout << i << " / " << viewpoint_count << std::endl;
    if (config_loader.intrinsic_config != 0) {
      camera.set_projection(camera.intrinsics()[i]);
    }

    glm::mat4
        mvp = camera.projection() * camera.model() * camera.extrinsics()[i];

    {
      // Shader 0: render I(pu, pv) = (tu, tv) to fbo
      fbo_uv.Bind();
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glUseProgram(program0.id());
      uniforms0.Bind("mvp", &mvp, 1);
      glBindVertexArray(args0.vao());
      glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);

      std::stringstream ss("");
      ss << output_path.c_str() << "/map_" << i << ".txt";
      // Read from framebuffer by .Capture() and save to file.
      EncodePixelToUV(ss.str(), fbo_uv.Capture());
    }

    {
      // Shader 1: render image to fbo
      fbo_image.Bind();
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glUseProgram(program1.id());
      input_texture.Bind(0);
      uniforms1.Bind("mvp", &mvp, 1);
      uniforms1.Bind("tex", GLuint(0));
      glBindVertexArray(args1.vao());
      glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
      glBindVertexArray(0);

      std::stringstream ss("");
      ss << output_path.c_str() << "/image_" << i << ".png";
      cv::imwrite(ss.str(), fbo_image.Capture());
    }

//    {
//      // Shader 2: simple rendering
//      window.Bind();
//      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//      glUseProgram(program2.id());
//      glActiveTexture(GL_TEXTURE1);
//      glBindTexture(GL_TEXTURE_2D, fbo_image.texture().id());
//      uniforms2.Bind("tex", GLuint(1));
//      glBindVertexArray(args2.vao());
//      glDrawArrays(GL_TRIANGLES, 0, 6);
//      glBindVertexArray(0);
//    }

    window.swap_buffer();
  }

  glfwTerminate();
  return 0;
}