//
// Created by Neo on 16/7/29.
//

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "cxxopts.h"
#include "glwrapper.h"

int main(int argc, char **argv) {
  /// Parse cmd line options
  cxxopts::Options options(argv[0], "Phong shading renderer (without texture)");
  options.positional_help("").show_positional_help();
  options.add_options()
      ("help", "show usages")
      ("w,width", "image width", cxxopts::value<int>()->default_value("640"))
      ("h,height", "image height", cxxopts::value<int>()->default_value("480"))
      ("n,name", "window name",
       cxxopts::value<std::string>()->default_value("Phong"))
      ("l,light", "path-to-config recording lighting conditions",
       cxxopts::value<std::string>()->default_value("lights.yaml"))
      ("m,model", "path-to-model (.obj)",
       cxxopts::value<std::string>()
           ->default_value("../../model/relief/original.obj"));

  auto result = options.parse(argc, argv);
  if (result.count("help")) {
    std::cout << options.help({"", "Group"}) << std::endl;
    return 0;
  }

  std::string path_to_model, path_to_light, window_name;
  int width = 640, height = 480;
  if (result.count("model")) {
    path_to_model = result["model"].as<std::string>();
  }
  if (result.count("light")) {
    path_to_light = result["light"].as<std::string>();
  }
  if (result.count("width")) {
    width = result["width"].as<int>();
  }
  if (result.count("height")) {
    height = result["height"].as<int>();
  }
  if (result.count("name")) {
    window_name = result["name"].as<std::string>();
  }

  /// Load models
  gl::Model model;
  model.LoadObj(path_to_model, false);
  gl::Light lights;
  lights.Load(path_to_light);

  /// Create OpenGL contexts
  gl::Window window(window_name, width, height);
  gl::Camera camera(window.visual_width(), window.visual_height());
  glm::mat4 model_mat = glm::mat4(1.0f);
  camera.set_model(model_mat);
  camera.SwitchInteraction(true);

  gl::Program program;
  program.Load("img_vtx_normal_uni_light_vert.glsl", gl::kVertexShader);
  program.ReplaceMacro("LIGHT_COUNT", lights.count_str, gl::kVertexShader);
  program.Load("img_vtx_normal_uni_light_frag.glsl", gl::kFragmentShader);
  program.ReplaceMacro("LIGHT_COUNT", lights.count_str, gl::kFragmentShader);
  program.Build();

  gl::Uniforms uniforms;
  uniforms.GetLocation(program.id(), "mvp", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "view", gl::kMatrix4f);
  uniforms.GetLocation(program.id(), "light", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_power", gl::kFloat);
  uniforms.GetLocation(program.id(), "ambient_coeff", gl::kVector3f);
  uniforms.GetLocation(program.id(), "specular_coeff", gl::kVector3f);
  uniforms.GetLocation(program.id(), "diffuse_color", gl::kVector3f);
  uniforms.GetLocation(program.id(), "light_color", gl::kVector3f);

  gl::Args args(3);
  args.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.positions().size(), model.positions().data());
  args.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                  model.normals().size(), model.normals().data());
  args.BindBuffer(2, gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                  model.indices().size(), model.indices().data());

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  do {
    /// Update viewpoint
    camera.UpdateView(window);
    glm::mat4 mvp = camera.mvp();//* view * m;
    glm::mat4 view = camera.view();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /// Specify program
    glUseProgram(program.id());
    uniforms.Bind("mvp", &mvp, 1);
    uniforms.Bind("view", &view, 1);
    uniforms.Bind("light", lights.positions.data(), lights.count);
    uniforms.Bind("light_power", &lights.power, 1);
    uniforms.Bind("ambient_coeff", &lights.ambient_coeff, 1);
    uniforms.Bind("specular_coeff", &lights.specular_coeff, 1);
    uniforms.Bind("diffuse_color", &lights.diffuse_color, 1);
    uniforms.Bind("light_color", &lights.light_color, 1);

    glBindVertexArray(args.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    window.swap_buffer();
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
      window.should_close() == 0);

  glfwTerminate();

  return 0;
}