//
// Created by Neo on 24/08/2017.
//

#include <fstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "glwrapper.h"
#include "rect.h"
#include <opencv2/opencv.hpp>

/// Pixel-unit
int kWindowWidth = 1280;
int kWindowHeight = 960;

/// Pixel-unit
int kTextureWidth = 2560;
int kTextureHeight = 1920;

bool kUnitVisual = false; // -> kUnitPixel

int main() {
  gl::Model model;
  model.LoadObj("../../model/f16/f16.obj");

  gl::Window window("F16", kWindowWidth, kWindowHeight, kUnitVisual);
  gl::Camera camera(window.visual_width(), window.visual_height());
  camera.SwitchInteraction(true);

  gl::Texture input_texture;
  input_texture.Init("../../model/f16/f16-1024.png");

  gl::Program program_pass1;
  program_pass1.Load("img_vtx_uv_uni_tex_vert.glsl", gl::kVertexShader);
  program_pass1.Load("img_vtx_uv_uni_tex_frag.glsl", gl::kFragmentShader);
  program_pass1.Build();

  gl::Uniforms uniforms_pass1;
  uniforms_pass1.GetLocation(program_pass1.id(), "mvp", gl::kMatrix4f);
  uniforms_pass1.GetLocation(program_pass1.id(), "tex", gl::kTexture2D);

  gl::Args args_pass1(3);
  args_pass1.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3),
                        model.positions().size(), model.positions().data());
  args_pass1.BindBuffer(1, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC2),
                        model.uvs().size(), model.uvs().data());
  args_pass1.BindBuffer(2,
                        gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                        model.indices().size(), model.indices().data());
  gl::FrameBuffer fbo_pass1(GL_RGBA, kTextureWidth, kTextureHeight,
                            kUnitVisual);

  gl::Program program_pass2;
  program_pass2.Load("img_uni_tex_vert.glsl", gl::kVertexShader);
  program_pass2.Load("img_uni_tex_frag.glsl", gl::kFragmentShader);
  program_pass2.Build();

  gl::Uniforms uniforms_pass2;
  uniforms_pass2.GetLocation(program_pass2.id(), "tex", gl::kTexture2D);

  gl::Args args_pass2(2);
  args_pass2.BindBuffer(0, gl::ArgAttrib(GL_ARRAY_BUFFER, GL_FLOAT_VEC3), 6,
                        (void *)kVertices);
  args_pass2.BindBuffer(1,
                        gl::ArgAttrib(GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT),
                        model.indices().size(), model.indices().data());

  glfwPollEvents();
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  do {
    camera.UpdateView(window);
    glm::mat4 mvp = camera.mvp();

    // Pass 1:
    // Render to framebuffer, into depth texture
    fbo_pass1.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program_pass1.id());
    input_texture.Bind(0);
    uniforms_pass1.Bind("mvp", &mvp, 1);
    uniforms_pass1.Bind("tex", GLuint(0));
    glBindVertexArray(args_pass1.vao());
    glDrawElements(GL_TRIANGLES, model.indices().size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    if (window.get_key(GLFW_KEY_ENTER) == GLFW_PRESS) {
      cv::imwrite("texture.png", fbo_pass1.Capture());
    }

    // Pass 2:
    // Test rendering texture
    window.Bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(program_pass2.id());
    fbo_pass1.texture().Bind(1);
    uniforms_pass2.Bind("tex", GLuint(1));
    glBindVertexArray(args_pass2.vao());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    window.swap_buffer();
  } while (window.get_key(GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           window.should_close() == 0);

  glfwTerminate();
  return 0;
}