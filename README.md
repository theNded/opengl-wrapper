# OpenGL Wrapper

Initially, this project was a wrapper for OpenGL 3.x+ pipeline, merely for 
easier use. Later, when I have to deal with various tasks, functions are 
added, and it becomes more comprehensive. There might be a lot of bugs, since
 it is designed for individual projects. 
 
 - The code that wraps OpenGL raw functions is located in `src`.
 - The example codes are located at `app`.
 - Related OpenGL shader code can be found in `shader`.
 - Models (.obj files) and textures (.png files) can be found in 
 [Google Drive](https://drive.google.com/open?id=1HmdqbntbyXL7M1rhBeCvPiObid02hYHb). It should be placed 
 in `model` in the root directory of the project. 


 ## Build and Run

 
 ### Dependencies
 
 - [CMake](https://cmake.org/)
 - [GLFW3](http://www.glfw.org/docs/latest/)
 - [GLEW](http://glew.sourceforge.net/)
 - [GLM](https://glm.g-truc.net/)
 - [OpenCV 2.4.x](https://opencv.org/)


 ### Compile
 
 ```bash
 mkdir build
 cd build
 cmake ..
 make -j4
 ```
 
### Run
Go to `./app`, navigate to directories, and run corresponding binaries.
Typically,

- 1~4_project_name are for test usage
- eth_project_name are utils for our decomposition project.

For each app, use `./app_name --help` to view usages.
Most of the OpenGL applications support navigation with keyboard.

- `WASD` for go front, left, behind, right 
- `Space` for up and `Shift` for down
- `IJKL` for rotate up, left, down, right

I am working on introducing mouse interactions.